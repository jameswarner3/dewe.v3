package net.qyjohn.dewev3.worker;

import java.io.*;
import java.nio.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import com.amazonaws.ClientConfiguration;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import org.apache.log4j.Logger;
import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.blob.*;

/**
 * Created by 44918054 on 5/06/2018.
 */
public class LambdaLocalDownloadThreadAzure extends Thread {

    // S3 side requirements
    //public DbxClientV2 client;
    //private DbxRequestConfig config;

    CloudStorageAccount storageAccount;
    CloudBlobClient blobClient = null;

    public static final String storageConnectionString =
            "DefaultEndpointsProtocol=https;" +
            "AccountName=dewestorage;" +
            "AccountKey=kneX2rws96VmvlaNTEdqpsElAx6b2KuoCks0I4fyPVmAjXU/tPbvnnhg1gPvX0ehw0grKo0ATOMkqVEcpQhZkg==;"+
            "EndpointSuffix=core.windows.net";

    private static final String ACCESS_TOKEN = "KImjRzLvWZcAAAAAAAACZfSiceVjBzSEIsLGbrvY-S4ENQeQpfiirifyOghP4jOU";
    // Local requirements
    public String tempDir = "/tmp";
    ConcurrentHashMap<String, Boolean> cachedFiles;
    ConcurrentLinkedQueue<String> downloadQueue;

    // Logging
    final static Logger logger = Logger.getLogger(LambdaLocalDownloadThread.class);

    public LambdaLocalDownloadThreadAzure(String tempDir, ConcurrentHashMap<String, Boolean> cachedFiles, ConcurrentLinkedQueue<String> downloadQueue)
    {
        this.tempDir = tempDir;
        this.cachedFiles = cachedFiles;
        this.downloadQueue = downloadQueue;

        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setMaxConnections(1000);
        clientConfig.setSocketTimeout(60*1000);
        storageAccount = CloudStorageAccount.parse(storageConnectionString);
        blobClient = storageAccount.createCloudBlobClient();

    }

    public void download_one(String job)
    {
        try
        {
            if (cachedFiles.get(job) == null)
            {
                // The filename comes in the following format
                // bucket|prefix|bin|filename
                // bucket|prefix|workdir|filename
                cachedFiles.put(job, new Boolean(false));
                String[] info = job.split("\\|");
                String bucket = info[0];
                String prefix = info[1];
                String folder = info[2];
                String file   = info[3];
                String key    = "/" + prefix + "/" + folder + "/" + file;
                String outfile= tempDir + "/" + file;

                // Download until success
//				logger.debug("Downloading " + job);
                logger.debug("Downloading " + key + " to " + outfile);
                boolean success = false;
                while (!success)
                {
                    try
                    {

                        OutputStream out = new FileOutputStream(outfile);

                        try {
                            File downloadFile = new File()
                            FileMetadata metadata = client.files().downloadBuilder(key).download(out);
                        } finally {
                            out.close();
                        }


                        if (folder.equals("bin"))	// Binary file
                        {
                            Process p = Runtime.getRuntime().exec("chmod +x " + outfile);
                            p.waitFor();
                        }

                        success = true;
                    } catch (Exception e1)
                    {
                        logger.error("Error downloading " + outfile);
                        logger.error("Retry after 1000 ms... ");
                        System.out.println(e1.getMessage());
                        e1.printStackTrace();
                        sleep(1000);
                    }
                }
                cachedFiles.put(job, new Boolean(true));
            }
            else
            {
                while (cachedFiles.get(job).booleanValue() == false)
                {
                    try
                    {
                        sleep(100);
                    } catch (Exception e)
                    {
                        System.out.println(e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void run()
    {
        while (true)
        {
            try
            {
                String file = downloadQueue.poll();
                if (file != null)
                {
                    download_one(file);
                }
                else
                {
                    sleep(1000);
                }
            } catch (Exception e)
            {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

}
