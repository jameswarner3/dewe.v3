package net.qyjohn.dewev3.worker;

import java.io.*;
import java.nio.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.DbxClientV2;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.services.s3.*;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.files.WriteMode;
import org.apache.log4j.Logger;

public class LambdaLocalUploadThread extends Thread
{
	// S3 side requirements
	public DbxClientV2 client;
	private DbxRequestConfig config;
	private static final String ACCESS_TOKEN = "KImjRzLvWZcAAAAAAAACZfSiceVjBzSEIsLGbrvY-S4ENQeQpfiirifyOghP4jOU";
	// Local requirements
	public String tempDir = "/tmp";
	ConcurrentHashMap<String, Boolean> cachedFiles;
	ConcurrentLinkedQueue<String> uploadQueue;

	// Logging
	final static Logger logger = Logger.getLogger(LambdaLocalUploadThread.class);
	 
	public LambdaLocalUploadThread(String tempDir, ConcurrentHashMap<String, Boolean> cachedFiles, ConcurrentLinkedQueue<String> uploadQueue)
	{
		this.tempDir = tempDir;
		this.cachedFiles = cachedFiles;
		this.uploadQueue = uploadQueue;

		ClientConfiguration clientConfig = new ClientConfiguration();
		clientConfig.setMaxConnections(1000);
		clientConfig.setSocketTimeout(60*1000);
		config = new DbxRequestConfig("DEWE.v3.1");
		client = new DbxClientV2(config, ACCESS_TOKEN);
	}

	public void upload_one(String job)
	{
		try
		{
			// The filename comes in the following format
			// bucket|prefix|bin|filename 
			// bucket|prefix|workdir|filename 
			cachedFiles.put(job, new Boolean(false));
			String[] info = job.split("\\|");
			String bucket = info[0];
			String prefix = info[1];
			String folder = info[2];
			String file   = info[3];

			String key  ="/" + prefix + "/" + folder + "/" + file;
			String filename = tempDir + "/" + file;

			logger.debug("Uploading " + filename + " to " + key);
			boolean success = false;
			while (!success)
			{
				try
				{
					InputStream in = new FileInputStream(filename);
					FileMetadata metadata = client.files().uploadBuilder(key).uploadAndFinish(in);
					cachedFiles.put(job, new Boolean(true));
					success = true;
				} catch (Exception e1)
				{
					logger.error("Error uploading " + file);
					logger.error("Retry after 1000 ms...");
					System.out.println(e1.getMessage());
					e1.printStackTrace();						
					sleep(1000);
				}
			}
		} catch (Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public void run()
	{
		while (true)
		{
			try
			{
				String file = uploadQueue.poll();
				if (file != null)
				{
					upload_one(file);
				}
				else
				{
					sleep(1000);
				}
			} catch (Exception e)
			{
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
	}
}
